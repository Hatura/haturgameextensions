#pragma once

#include <string>

// Include subfunctionalities as dependency-style
#include "HStateMachine/hstatemachine.h"

namespace HE {

class HGameExtensions {
public:
    HGameExtensions();

    static std::string GetVersion();

private:
    static const std::string m_libVersion;
};

} // namespace HE
