#pragma once

#include <functional>
#include <vector>
#include <queue>
#include <condition_variable>
#include <future>

#include "hworkerthread.h"

namespace HE {
namespace Thread {

template<typename R>
/**
 * @brief IsFutureReady - Helper function to determine if a task via queue (or a future in general) is completed yet
 */
bool IsFutureReady(const std::future<R>& f) {
    return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
}

class HThreadPool
{
public:
    HThreadPool();
    ~HThreadPool();

    void Queue(std::function<void()> function);

    /**
     * @brief Queue - Add task to worker queue
     * @param f
     * @param args
     * @return = std::future
     */
    template<class F, class... Args>
    auto Queue(F&& f, Args&&... args) -> std::future<typename std::result_of<F(Args...)>::type> {
        using return_type = std::result_of<F(Args...)>::type;

        auto task = std::make_shared<std::packaged_task<return_type()>> (std::bind(std::forward<F>(f), std::forward<Args>(args)...));

        std::future<return_type> res = task->get_future();
        {
            std::unique_lock<std::mutex> lock(m_conditionMutex);

            if (m_shutdownRequested) {
                throw std::runtime_error("Enqueueing on stopped HThreadPool");
            }

            m_queue.emplace([task] () { (*task)(); });
        }

        m_conditionVar.notify_one();
        return res;
    }

private:
    unsigned int m_numCores;

    bool m_shutdownRequested;

    std::condition_variable m_conditionVar;

    std::mutex m_conditionMutex;

    std::vector<std::thread> m_workerThreads;
    std::queue<std::function<void()>> m_queue;
};

} // namespace Thread
} // namespace HE
