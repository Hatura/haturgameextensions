#include "hthreadpool.h"

#include <thread>

namespace HE {
namespace Thread {

HThreadPool::HThreadPool()
    : m_numCores(std::thread::hardware_concurrency())
    , m_shutdownRequested(false) {

    for (unsigned int i = 0; i < m_numCores; ++i) {
        m_workerThreads.emplace_back(std::thread([this] {
            while (true) {
                std::function<void()> task;

                {
                    std::unique_lock<std::mutex> lock(m_conditionMutex);
                    m_conditionVar.wait(lock, [this] {
                        return m_shutdownRequested || !m_queue.empty();
                    });
                    if (m_shutdownRequested && m_queue.empty()) {
                        break;
                    }
                    task = std::move(m_queue.front());
                    m_queue.pop();
                }

                task();
            }
        }));
    }
}

HThreadPool::~HThreadPool() {
    {
        std::lock_guard<std::mutex> lock(m_conditionMutex);
        m_shutdownRequested = true;
    }

    // Is this enough?
    m_conditionVar.notify_all();

    for (auto& thread : m_workerThreads) {
        thread.join();
    }
}

void HThreadPool::Queue(std::function<void ()> function) {
    if (m_numCores == 0 || m_numCores == 1) {
        // Single core or not detectable, just execute directly

        function();

    } else {
        m_queue.push(std::move(function));
        m_conditionVar.notify_one();
    }
}

} // namespace Thread
} // namespace HE
