#include "hgameextensions.h"

namespace HE {

const std::string HGameExtensions::m_libVersion = "0.0.1a";

HGameExtensions::HGameExtensions() {

}

std::string HGameExtensions::GetVersion() {
    return m_libVersion;
}

} // namespace HE
