#include "hstatemachine.h"

#include <assert.h>
#include <iostream>

namespace HE {
namespace State {

HStateMachine::HStateMachine()
    : m_initialized(false)
    , m_previousState(nullptr)
    , m_currentState(nullptr) {

}

HState& HStateMachine::RegisterState(HState state) {
    m_registeredStates.push_back(std::move(state));

    return m_registeredStates.back();
}

void HStateMachine::Initiate(HState& state) {
    assert(!m_initialized);

    m_currentState = &state;
    m_initialized = true;
}

void HStateMachine::Update() {
    bool doTransition = false;

    if (m_currentState != nullptr) {
        m_currentState->OnState();

        doTransition = m_currentState->UpdateTransitions();
    }

    if (doTransition) {
        HTransition::ETransitionType type = m_currentState->GetBufferedTransition()->GetTransitionType();

        if (type == HTransition::ETransitionType::TRANSITION_DESTINATION) {
            HState* newState = m_currentState->GetBufferedTransition()->GetDestinationState();

            m_currentState->OnExit();

            m_previousState = m_currentState;
            m_currentState = newState;

            m_currentState->OnEnter();
        }
        else if (type == HTransition::ETransitionType::TRANSITION_RESTORE_BEHAVIOUR) {
            m_currentState->OnExit();

            HState* swapBuffer = m_currentState;
            m_currentState = m_previousState;
            m_previousState = swapBuffer;

            m_currentState->OnEnter();
        }
    }
}

const std::string HStateMachine::GetCurrentStateName() const {
    if (m_currentState == nullptr || m_currentState->GetCurrentStateName() == std::string("")) {
        return std::string("NO STATE!");
    }
    else {
        return m_currentState->GetCurrentStateName();
    }
}

} // namespace State
} // namespace HE
