#pragma once

#include <functional>
#include <deque>
#include "htransition.h"

namespace HE {
namespace State {

class HState final
{
public:
    explicit HState(const std::string& stateName = std::string("Unnamed state"));

    // Give HStateMachine and only HStateMachine access to various private functions
    friend class HStateMachine;

    HTransition& RegisterTransition(HTransition transition);

    void SetEnterFunction(std::function<void()> enterFunction);
    void SetStateFunction(std::function<void()> stateFunction);
    void SetExitFunction(std::function<void()> exitFunction);

    const std::string& GetCurrentStateName() const;

private:
    std::string m_stateName;

    std::function<void()> m_enterFunction;
    std::function<void()> m_stateFunction;
    std::function<void()> m_exitFunction;

    // Can't use vector here because RegisterTransition is returning and vector reallocates memory on push_back
    std::deque<HTransition> m_transitions;

    HTransition* m_bufferedSuccessfullTransition;

    void OnEnter() const;
    void OnState() const;
    void OnExit() const;

    // Returns nullptr if no transition is to be made, returns new State if Transition registered StateSwitch
    bool UpdateTransitions();
    const HTransition* GetBufferedTransition() const;
};

} // namespace State
} // namespace HE
