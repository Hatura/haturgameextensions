#pragma once

#include <functional>

namespace HE {
namespace State {

class HState;

class HTransition final
{
public:
    // Constructor for a destination transition, if function returns true then destinationState is entered
    HTransition(HState* destinationState, std::function<bool()> transitionFunction);

    // Constructor for a restore behaviour transition, if function return true HStateManager returns to previous State
    explicit HTransition(std::function<bool()> transitionFunction);

    friend class HState;

    enum class ETransitionType {
        TRANSITION_DESTINATION,
        TRANSITION_RESTORE_BEHAVIOUR
    };

    ETransitionType GetTransitionType() const;
    HState* GetDestinationState() const;

private:
    bool m_restoreBehaviourTransition;

    std::function<bool()> m_transitionFunction;

    HState* m_destinationState;

    ETransitionType m_transitionType;

    bool Update();
};

} // namespace State
} // namespace HE
