#pragma once

#include <deque>
#include "hstate.h"
#include "htransition.h"

namespace HE {
namespace State {

/**
 * @brief The HStateMachine class
 *
 * A state machine used in AI / Input and other things
 */
class HStateMachine final
{
public:
    HStateMachine();

    HState& RegisterState(HState state);

    /**
     * @brief Initiate
     * @param state
     *
     * Sets the initial state
     */
    void Initiate(HState& state);

    /**
     * @brief Update
     *
     * Updates the StateMachine
     */
    void Update();

    const std::string GetCurrentStateName() const;

private:
    bool m_initialized;

    HState* m_previousState;
    HState* m_currentState;

    // Can't use a vector here, because it invalidates pointers, iterators, references on push_back (resize)
    std::deque<HState> m_registeredStates;
};

} // namespace State
} // namespace HE
