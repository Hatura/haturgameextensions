#include "hstate.h"

namespace HE {
namespace State {

HState::HState(const std::string& stateName)
    : m_stateName(stateName)
    , m_enterFunction([] () { })
, m_stateFunction([] () { })
, m_exitFunction([] () { })
, m_bufferedSuccessfullTransition(nullptr) {

}

HTransition& HState::RegisterTransition(HTransition transition) {
    m_transitions.push_back(std::move(transition));

    return m_transitions.back();
}

bool HState::UpdateTransitions() {
    for (auto& transition : m_transitions) {
        if (transition.Update()) {
            // Successful
            m_bufferedSuccessfullTransition = &transition;
            return true;
        }
    }

    return false;
}

const HTransition* HState::GetBufferedTransition() const {
    return m_bufferedSuccessfullTransition;
}

const std::string& HState::GetCurrentStateName() const {
    return m_stateName;
}

void HState::SetEnterFunction(std::function<void ()> enterFunction) {
    m_enterFunction = std::move(enterFunction);
}

void HState::SetStateFunction(std::function<void ()> stateFunction) {
    m_stateFunction = std::move(stateFunction);
}

void HState::SetExitFunction(std::function<void ()> exitFunction) {
    m_exitFunction = std::move(exitFunction);
}

void HState::OnEnter() const {
    m_enterFunction();
}

void HState::OnState() const {
    m_stateFunction();
}

void HState::OnExit() const {
    m_exitFunction();
}

} // namespace State
} // namespace HE
