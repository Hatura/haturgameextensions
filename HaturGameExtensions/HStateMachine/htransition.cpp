#include "htransition.h"

#include <assert.h>
#include "hstate.h"

namespace HE {
namespace State {

HTransition::HTransition(HState* destinationState, std::function<bool()> transitionFunction)
    : m_destinationState(destinationState)
    , m_transitionFunction(transitionFunction)
    , m_restoreBehaviourTransition(false)
    , m_transitionType(ETransitionType::TRANSITION_DESTINATION){

}

HTransition::HTransition(std::function<bool()> transitionFunction)
    : m_destinationState(nullptr)
    , m_transitionFunction(transitionFunction)
    , m_restoreBehaviourTransition(true)
    , m_transitionType(ETransitionType::TRANSITION_RESTORE_BEHAVIOUR) {

}

bool HTransition::Update() {
    if (m_transitionFunction()) {
        return true;
    }
    return false;
}

HTransition::ETransitionType HTransition::GetTransitionType() const {
    return m_transitionType;
}

HState* HTransition::GetDestinationState() const {
    assert(m_destinationState != nullptr);

    return m_destinationState;
}

} // namespace State
} // namespace HE
