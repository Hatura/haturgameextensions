#pragma once

#include <string>
#include <memory>
#include <mutex>

namespace HE {
namespace Res {

class HResource {
public:
    HResource()
        : m_loaded(false)
        , m_useCount(1)
        , m_hasDefault(false) { }
    virtual ~HResource() = 0 { }

    bool IsLoaded() const;
    int GetUseCount() const;
    bool HasDefault() const;

    friend class HResourceManager;

protected:
    void SetLoaded();

private:
    // Called from ResourceManager, specify here how the resource is created
    virtual bool OnCreation(const std::string& fileName) = 0;

    void IncreaseUseCount(int amount);
    void DecreaseUseCount(int amount);

    bool m_loaded;
    int m_useCount;
    bool m_hasDefault;

    mutable std::mutex m_mutex;
};

class HFileResource : public HResource {
public:
    const std::string& Get() const;

    friend class HResourceManager;

private:
    bool OnCreation(const std::string& fileName) override;

    std::string m_string;
};

} // namespace Res
} // namespace HSF
