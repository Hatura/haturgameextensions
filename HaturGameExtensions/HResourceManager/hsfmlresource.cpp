#include "hsfmlresource.h"

namespace HE {
namespace Res {

std::unique_ptr<sf::Texture> HSFMLResource::m_defaultTexture = nullptr;

HSFMLResource::HSFMLResource() {

}

void HSFMLResource::SetDefaultTexture(std::unique_ptr<sf::Texture> defaultTexture) {
    m_defaultTexture = std::move(defaultTexture);
}

void HSFMLResource::SetDefaultTexture(const std::string& fileName) {
    std::unique_ptr<sf::Texture> texture(new sf::Texture());
    if (texture->loadFromFile(fileName)) {
        m_defaultTexture = std::move(texture);
    }
}

const sf::Texture* HSFMLResource::Get() const {
    return m_texture.get();
}

bool HSFMLResource::OnCreation(const std::string& fileName) {
    m_texture = std::unique_ptr<sf::Texture>(new sf::Texture());
    if (m_texture->loadFromFile(fileName)) {
        SetLoaded();
        return true;
    } else {
        return false;
    }
}

const sf::Texture* HSFMLResource::GetDefaultTexture() const {
    return m_defaultTexture.get();
}

} // namespace Res
} // namespace HE
