#pragma once

#include <unordered_map>
#include <unordered_set>
#include <set>
#include <mutex>
#include <fstream>
#include <stdio.h>

namespace HE {
namespace Res {

enum class EAllocatorFileNotFoundBehavior {
    Exception,          // throw exception
    Default,            // return default defined per allocator
    ManagerDefault      // return default defined per ResourceManager
};

class HResourceManager;
class HResource;

/**
 * @brief The HResourceAllocator class
 *
 * - Acquire a HResourceAllocator through your registered HResourceManager by calling HResourceManager::CreateAllocator
 * - The first paramter of the CreateAllocator method defines the fetching method if you are using the Manager asynchronosly,
 * if you fetch async the Thread will invoke the invokerFunctions and this may lead to race conditions if the Resource is accessed
 * in any other way in your code. If you set it to false (default behavior) you need to call HResourceAllocator::ExecutePendingActions()
 * every Update in the main thread to safely call them.
 * - If you you HResourceAllocator::LoadAsync you can use the returnvalue if a default is specified. This allows you to use placeholder
 * textures while the resource is being loaded (If you are using this with SFML you need to rescale the sprite depending on the texture size)
 * - You also need to be aware that you need to manually dynamic_cast the HResource returned on invoker functions, this is a cost but a small
 * one for asynchronos loading.
 */
class HResourceAllocator {
public:
    ~HResourceAllocator();

    friend class HResourceManager;

    template <typename T>
    T* Load(const std::string& fileName) {
        // Check if file exists..
        bool fileExists = false;
        if (m_resourceManager->IsFileAlreadyAssigned(fileName)) {
            // First we check if it's already stored, so it has to be existant..
            // (unless someone deletes accessfiles while running the program)
            fileExists = true;
        } else {
            // fopen is the fastest way to check for existance using no POSIX functions (not possible)..
            std::lock_guard<std::mutex> fileLock(m_checkFileMutex);
            FILE* file = fopen(fileName.c_str(), "r");
            if (file != nullptr) {
                fileExists = true;
            }
            fclose(file);
        }

        if (fileExists) {
            // Get Resource from ResourceManager in a non-async way.
            T* resource = m_resourceManager->AcquireResource<T>(fileName);

            // Store local information for deletion..
            {
                std::lock_guard<std::mutex> lock(m_localResourcesMutex);
                auto it = m_localResources.find(fileName);
                if (it == m_localResources.end()) {
                    m_localResources.insert(std::make_pair(fileName, 1));
                }
                else {
                    it->second++;
                }
            }

            return resource;
        }
        else {
            if (m_allocatorBehavior == EAllocatorFileNotFoundBehavior::Exception) {
                // throw exception
                std::string exceptionString = std::string("File " + fileName + " not found!");
                throw std::exception(exceptionString.c_str());
            }
            else if (m_allocatorBehavior == EAllocatorFileNotFoundBehavior::Default) {
                // return local default
                return nullptr;
            }
            else if (m_allocatorBehavior == EAllocatorFileNotFoundBehavior::ManagerDefault) {
                // return manager default
                return nullptr;
            }
        }
    }


    /**
     * @brief LoadAsync
     * @param fileName
     * Returns placeholder if behavior for that is defined
     */
    template <typename T>
    void LoadAsync(const std::string& fileName, std::function<void(const HResource* resource)> invokerFunction) {
        // This is not done async .. if we plan to do it we need to move this and the same code from the non async version to manager
        // also the exception throwing may be a problem?
        bool fileExists = false;
        if (m_resourceManager->IsFileAlreadyAssigned(fileName)) {
            // First we check if it's already stored, so it has to be existant..
            // (unless someone deletes accessfiles while running the program)
            fileExists = true;
        } else {
            // fopen is the fastest way to check for existance using no POSIX functions (not possible)..
            std::lock_guard<std::mutex> fileLock(m_checkFileMutex);
            FILE* file = fopen(fileName.c_str(), "r");
            if (file != nullptr) {
                fileExists = true;
            }
            fclose(file);
        }

        if (fileExists) {
            T* resource = m_resourceManager->AcquireResourceAsync<T>(this, fileName, std::move(invokerFunction));

            {
                std::lock_guard<std::mutex> lock(m_localAsyncMutex);
                m_localAsyncRequests.insert(fileName);
            }

//            return resource;
        }
        else {
//            if (m_allocatorBehavior == EAllocatorFileNotFoundBehavior::Exception) {
                // throw exception
                std::string exceptionString = std::string("File " + fileName + " not found!");
                throw std::exception(exceptionString.c_str());
//            }
//            else if (m_allocatorBehavior == EAllocatorFileNotFoundBehavior::Default) {
//                // return local default
//                return nullptr;
//            }
//            else if (m_allocatorBehavior == EAllocatorFileNotFoundBehavior::ManagerDefault) {
//                // return manager default
//                return nullptr;
//            }
        }
    }

    EAllocatorFileNotFoundBehavior GetAllocatorFileNotFoundBehavior() const {
        return m_allocatorBehavior;
    }

    /**
     * @brief ExecutePendingActions
     *
     * This does nothing if the thread is fetching async..
     */
    void ExecutePendingActions();

    inline bool IsFetchingAsync() const {
        return m_fetchAsync;
    }

    inline unsigned int GetID() const {
        return m_id;
    }
private:
    HResourceAllocator(HResourceManager* resourceManager, bool fetchAsync, EAllocatorFileNotFoundBehavior behavior)
        : m_resourceManager(resourceManager)
        , m_fetchAsync(fetchAsync)
        , m_allocatorBehavior(behavior) {

        // We need a ID to compare Objects on yield
        static int currentID = 0;
        m_id = currentID;
        ++currentID;
    }

    /**
     * @brief OnAsyncResourceInsertion
     *
     * Called when a resource is successfully async loaded
     */
    void OnAsyncResourceInsertion(const std::string& fileName);

    void PushToFunctionQueue(std::function<void(const HResource*)> function, const HResource* resource);

    unsigned int m_id;

    HResourceManager* m_resourceManager;

    bool m_fetchAsync;

    // This is the file not found behavior, it has nothing to do with the async behavior
    EAllocatorFileNotFoundBehavior m_allocatorBehavior;

    std::unordered_map<std::string, int> m_localResources;

    std::mutex m_checkFileMutex;
    std::mutex m_localResourcesMutex;
    std::mutex m_localAsyncMutex;
    std::mutex m_functionQueueMutex;

    // This is not cleaned up after requests are handled in ResourceManager atm, TODO!
    std::unordered_multiset<std::string> m_localAsyncRequests;

    // Funciton queue, if assignment is requested to be in the main thread, not the worker
    std::vector<std::pair<std::function<void(const HResource*)>, const HResource*>> m_functionQueue;
};

} // namespace Res
} // namespace HSF
