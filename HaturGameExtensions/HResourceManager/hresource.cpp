#include "hresource.h"

namespace HE {
namespace Res {

bool HResource::IsLoaded() const {
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_loaded;
}

int HResource::GetUseCount() const {
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_useCount;
}

bool HResource::HasDefault() const {
    return m_hasDefault;
}

void HResource::SetLoaded() {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_loaded = true;
}

void HResource::IncreaseUseCount(int amount) {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_useCount += amount;
}

void HResource::DecreaseUseCount(int amount) {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_useCount -= amount;
}

const std::string& HFileResource::Get() const {
    if (IsLoaded()) {
        return m_string;
    } else {
        throw std::runtime_error("Trying to access resource not loaded!");
    }
}

bool HFileResource::OnCreation(const std::string& fileName) {
    m_string = std::string("Fuck my life..");
    SetLoaded();

    return true;
}


} // namespace Res
} // namespace HSF
