#pragma once

#include <unordered_map>
#include <set>
#include <queue>
#include <memory>
#include <thread>
#include <condition_variable>
#include <typeindex>
#include <typeinfo>

#define HRESDEBUG

#ifdef HRESDEBUG
#include <iostream>
#endif

#include "hresourceallocator.h"
#include "hresource.h"

namespace HE {
namespace Res {

class HResourceManager
{
public:
    /**
     * @brief HResourceManager
     * Constructor for owned thread
     */
    HResourceManager(bool supportThreading = true);
    ~HResourceManager();

    friend class HResourceAllocator;

    std::unique_ptr<HResourceAllocator> CreateAllocator(bool fetchAsync = false, EAllocatorFileNotFoundBehavior behavior = EAllocatorFileNotFoundBehavior::Exception);

private:
    // Helper struct/class for async requested resources
    struct AsyncRequestContent {
        friend class HResourceManager;

        const inline std::string& GetFileName() const {
            return m_fileName;
        }

        inline HResourceAllocator* GetAllocator() const {
            return m_allocator;
        }

        inline unsigned int GetAllocatorID() const {
            return m_allocator->GetID();
        }

    private:
        AsyncRequestContent(HResourceAllocator* allocator, const std::string& fileName, std::function<void(const HResource* resource)> invokerFunction)
            : m_allocator(allocator)
            , m_fileName(fileName)
            , m_resource(nullptr)
            , m_invokerFunction(invokerFunction) { }

        /**
         * @brief PreCreate
         * Creates the resource object, but as raw class, the data itself is not initialized yet.
         */
        template <typename T>
        void PreCreate() {
            if (m_resource == nullptr) {
                m_resource = std::unique_ptr<T>(new T());
            }
        }

        HResourceAllocator*                             m_allocator;
        std::string                                     m_fileName;
        std::unique_ptr<HResource>                      m_resource;
        std::function<void(const HResource* resource)>  m_invokerFunction;
    };

    template <typename T>
    T* AcquireResource (const std::string& fileName) {
        std::unique_lock<std::mutex> resLock(m_resourcesMutex, std::defer_lock);
        std::unique_lock<std::mutex> asyncReqLock(m_asyncReqMutex, std::defer_lock);

        std::lock(resLock, asyncReqLock);

        // The following code dealt with resources that were previously already requested in an async way but not yet loaded
        // It should be unnessary now because After loading async the Thread checks again if it is loaded non-async yet

//        auto ait = m_asyncRequested.end();

//        for (ait = m_asyncRequested.begin(); ait != m_asyncRequested.end(); ++ait) {
//            if ((*ait)->GetFileName() == fileName) {
//                break;
//            }
//        }

//        if (ait != m_asyncRequested.end()) {
//            // Accessing resource that was previously loaded async but is not yet loaded.. implement later / TODO

//            // Unlock before throwing
//            resLock.unlock();
//            asyncReqLock.unlock();
//            throw std::runtime_error("Accessing resource loaded async, behavior not yet implemented");
//        }

        auto it = m_resources.find(fileName);

        if (it == m_resources.end()) {
            // new
            std::unique_ptr<T> resource(new T);
            resource->OnCreation(fileName);
            m_resources.insert(std::make_pair(fileName, std::move(resource)));

#ifdef HRESDEBUG
            std::cout << "Acquired new " << fileName << std::endl;
#endif

            // return placeholder
            return dynamic_cast<T*>(m_resources.find(fileName)->second.get());
        }
        else {
            // increase
            it->second->m_useCount++;

#ifdef HRESDEBUG
            std::cout << "Increased useCount of " << fileName << std::endl;
#endif

            return dynamic_cast<T*>(it->second.get());
        }
    }

    /**
     * @brief AcquireAsync
     * @param fileName
     * @param invokerFunction = Called on finishing loading, the invokerfunction should assign the new object if it needs reassignment
     *
     * Load a resource asynchron in a thread, invoke function after load is done (user has to do resource binding)
     * Returns a placeholder if defined
     */
    template <typename T>
    T* AcquireResourceAsync(HResourceAllocator* allocator, const std::string& fileName, std::function<void(const HResource* resource)> invokerFunction) {
        if (!m_supportThreading) {
            throw std::runtime_error("Requested a async load, but ResourceManager is instructed to not support async");
        }
        else {
            {
                std::unique_lock<std::mutex> resLock(m_asyncReqMutex);

                std::unique_ptr<AsyncRequestContent> asyncRequestContent(new AsyncRequestContent(allocator, fileName, std::move(invokerFunction)));
                asyncRequestContent->PreCreate<T>();
                m_asyncRequested.push_back(std::move(asyncRequestContent));

                resLock.unlock();

                if (m_ownedThread != nullptr) {
                    m_conditionVariable.notify_one();
                }
            } 
        }
    }

    /**
     * @brief YieldResources
     * @param allocatorID
     * @param resources
     * Sets all resources free, HResourceAllocator calls this..
     */
    void YieldResources(unsigned int allocatorID, const std::unordered_map<std::string, int>& resources, const std::unordered_multiset<std::string>& localAsyncRequests);

    /**
     * @brief Idle
     *
     * Function called by owned thread object to acquire resources async
     */
    void Idle();

    /**
     * @brief IsFileAlreadyAssigned
     * @param fileName
     * Helper to avoid fopen
     */
    bool IsFileAlreadyAssigned(const std::string& fileName);

    // The stored resources
    std::unordered_map<std::string, std::unique_ptr<HResource>> m_resources;

    bool m_supportThreading;

    // Thread used for async operations if no thread is supplemented by constructor (not yet implemented)
    std::unique_ptr<std::thread> m_ownedThread;

    // Thread mutexes
    std::mutex m_resourcesMutex;    // m_resources
    std::mutex m_asyncReqMutex;     // m_asyncRequested & m_asyncQueue
    std::mutex m_shutdownMutex;     // m_asyncShutdown;

    std::mutex m_conditionMutex;    // for condition variable

    // Condition variable for the thread, notify on new data
    std::condition_variable m_conditionVariable;

    // Async requested resources, maybe there is a better solution to multiset but it's getting rather complicated
    std::vector<std::unique_ptr<AsyncRequestContent>> m_asyncRequested;

    // Order of the async requested resources, queue is a FIFO container
    // std::queue<std::pair<std::string, std::function<void()>>> m_asyncQueue;

    bool m_asyncShutdown;
};

}
}
