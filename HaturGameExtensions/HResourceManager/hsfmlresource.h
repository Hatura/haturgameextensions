#pragma once

#include "hresource.h"

#include <SFML/Graphics/Texture.hpp>
#include <iostream>

namespace HE {
namespace Res {

/**
 * @brief The HSFMLResource class
 *
 * This class is dependent on the SFML static libraries and headers being available if you use this at runtime
 */

class HSFMLResource : public HResource
{
public:
    HSFMLResource();

    friend class HResourceManager;

    static void SetDefaultTexture(std::unique_ptr<sf::Texture> defaultTexture);
    static void SetDefaultTexture(const std::string& fileName);

    const sf::Texture* Get() const;

private:
    bool OnCreation(const std::string& fileName);

    const sf::Texture* GetDefaultTexture() const;

    std::unique_ptr<sf::Texture> m_texture;

    static std::unique_ptr<sf::Texture> m_defaultTexture;
};

} // namespace Res
} // namespace HE
