#include "hresourceallocator.h"

#include "hresourcemanager.h"

namespace HE {
namespace Res {

HResourceAllocator::~HResourceAllocator() {
    // Tell HResourceManager to free all resources this Allocator has gathered
    m_resourceManager->YieldResources(m_id, m_localResources, m_localAsyncRequests);

#ifdef HRESDEBUG
    std::unique_lock<std::mutex> localResLock(m_localResourcesMutex, std::defer_lock);
    std::unique_lock<std::mutex> localAsyncReqLock(m_localAsyncMutex, std::defer_lock);

    std::lock(localResLock, localAsyncReqLock);
    std::cout << "Removed HResourceAllocator, had " << m_localResources.size() << " resources registered and " << m_localAsyncRequests.size() << " requests still pending" << std::endl;
#endif
}

void HResourceAllocator::OnAsyncResourceInsertion(const std::string& fileName) {    
    std::unique_lock<std::mutex> localResLock(m_localResourcesMutex, std::defer_lock);
    std::unique_lock<std::mutex> localAsyncReqLock(m_localAsyncMutex, std::defer_lock);

    std::lock(localResLock, localAsyncReqLock);

    // Insert into Resources
    auto it = m_localResources.find(fileName);
    if (it == m_localResources.end()) {
        m_localResources.insert(std::make_pair(fileName, 1));
    } else {
        it->second += 1;
    }

    // Remoev from Requests
    auto ita = m_localAsyncRequests.find(fileName);
    if (ita != m_localAsyncRequests.end()) {
        m_localAsyncRequests.erase(ita);
    }
}

void HResourceAllocator::PushToFunctionQueue(std::function<void (const HResource*)> function, const HResource* resource) {
    std::lock_guard<std::mutex> lock(m_functionQueueMutex);

    m_functionQueue.push_back(std::make_pair(std::move(function), resource));
}

void HResourceAllocator::ExecutePendingActions() {
    std::lock_guard<std::mutex> lock(m_functionQueueMutex);

    for (auto& function : m_functionQueue) {
        function.first(function.second);
    }

    m_functionQueue.clear();
}

} // namespace Res
} // namespace HSF
