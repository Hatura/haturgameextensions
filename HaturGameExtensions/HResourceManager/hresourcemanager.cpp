#include "hresourcemanager.h"

#include <chrono>

namespace HE {
namespace Res {

HResourceManager::HResourceManager(bool supportThreading)
    : m_supportThreading(supportThreading)
    , m_asyncShutdown(false) {

    if (supportThreading) {
        m_ownedThread = std::unique_ptr<std::thread>(new std::thread([this] () {
            Idle();
        }));
    }
}

HResourceManager::~HResourceManager() {
    if (m_ownedThread != nullptr) {
        // Make worker thread join if one defined
        m_asyncShutdown = true;
        m_conditionVariable.notify_one();
        m_ownedThread->join();
    }
}

std::unique_ptr<HResourceAllocator> HResourceManager::CreateAllocator(bool fetchAsync, EAllocatorFileNotFoundBehavior behavior) {
    std::unique_ptr<HResourceAllocator> allocator(new HResourceAllocator(this, fetchAsync, behavior));

    return std::move(allocator);
}

void HResourceManager::YieldResources(unsigned int allocatorID, const std::unordered_map<std::string, int>& resources, const std::unordered_multiset<std::string>& localAsyncRequests) {
    std::unique_lock<std::mutex> resLock(m_resourcesMutex, std::defer_lock);
    std::unique_lock<std::mutex> asyncReqLock(m_asyncReqMutex, std::defer_lock);

    std::lock(resLock, asyncReqLock);

    for (const auto& resource : resources) {
        auto it = m_resources.find(resource.first);
        it->second->DecreaseUseCount(resource.second);
        if (it->second->m_useCount < 1) {
            m_resources.erase(resource.first);
#ifdef HRESDEBUG
            std::cout << "Removed " << resource.first << std::endl;
#endif
        }
#ifdef HRESDEBUG
        else {
            std::cout << "Reduced " << resource.first << " count by " << resource.second << std::endl;
        }
#endif
    }

    // Handle deleting for async requests
    // This may be really slow for large allocators
    for (auto& localAsyncRequest : localAsyncRequests) {
        m_asyncRequested.erase(std::remove_if(m_asyncRequested.begin(), m_asyncRequested.end(), [&] (std::unique_ptr<AsyncRequestContent>& managerAsyncRequests) {
            bool deleteThis = allocatorID == managerAsyncRequests->GetAllocatorID() && localAsyncRequest == managerAsyncRequests->GetFileName();
#ifdef HRESDEBUG
            if (deleteThis) {
                std::cout << "Deleted " << managerAsyncRequests->GetFileName() << " from async Requests" << std::endl;
            }
#endif
            return deleteThis;
        }), m_asyncRequested.end());
    }
}

void HResourceManager::Idle() {
    if (m_supportThreading) {
        while (true) {
            std::unique_lock<std::mutex> lock(m_conditionMutex);
            m_conditionVariable.wait(lock, [this] () {
#ifdef HRESDEBUG
                std::cout << "Checking async work.." << std::endl;
#endif
                std::unique_lock<std::mutex> asyncResLock(m_asyncReqMutex, std::defer_lock);
                std::unique_lock<std::mutex> shutdownLock(m_shutdownMutex, std::defer_lock);
                std::lock(asyncResLock, shutdownLock);
                bool doWork = m_asyncRequested.size() > 0 || m_asyncShutdown;
#ifdef HRESDEBUG
                std::cout << "Found work: " << doWork << std::endl;
#endif
                return doWork;
            });

#ifdef HRESDEBUG
            std::cout << "Found async work.." << std::endl;
#endif
            {
                std::lock_guard<std::mutex> shutdownLock(m_shutdownMutex);
                if (m_asyncShutdown) {
                    break;
                }
            }
            {
                std::unique_lock<std::mutex> resLock(m_resourcesMutex, std::defer_lock);
                std::unique_lock<std::mutex> asyncReqLock(m_asyncReqMutex, std::defer_lock);

                std::lock(resLock, asyncReqLock);
                while (true) {
                    { // Check if we really want to assign it.. or if join is requested
                        std::lock_guard<std::mutex> shutdownLock(m_shutdownMutex);
                        if (m_asyncShutdown) {
                            break;
                        }
                    }

                    bool foundMatch = false;

                    std::unique_ptr<AsyncRequestContent> workContent = nullptr;
                    auto deleterIT = m_asyncRequested.end();

                    // Iterate over the requests..
                    for (auto asyncIT = m_asyncRequested.begin(); asyncIT != m_asyncRequested.end(); ++asyncIT) {
                        auto it = m_resources.find((*asyncIT)->GetFileName());
                        if (it != m_resources.end()) {
                            // Resource is already existant..
                            it->second->IncreaseUseCount(1);
#ifdef HRESDEBUG
                            std::cout << "Increased use count async" << std::endl;
#endif
                            AsyncRequestContent* content = (*asyncIT).get();

                            if (!content->GetAllocator()->IsFetchingAsync()) {
                                content->GetAllocator()->PushToFunctionQueue(std::move(content->m_invokerFunction), it->second.get());
                            }
                            else {
                                content->m_invokerFunction(it->second.get());
                            }

                            content->GetAllocator()->OnAsyncResourceInsertion(content->GetFileName());

                            deleterIT = asyncIT;

                            foundMatch = true;

                            break;
                        }
                        else {
                            // Resource is not existant, save a "copy" to workContent to work with it later
                            workContent = std::move(*asyncIT);

                            deleterIT = asyncIT;

                            foundMatch = true;

                            break;
                        }
                    }

                    if (!foundMatch) {
                        break; // No matches, next..
                    }

                    if (deleterIT != m_asyncRequested.end()) {
                        // Can delete here, content was moved to workContent
                        m_asyncRequested.erase(deleterIT);
                    }

                    if (workContent != nullptr) {
                        asyncReqLock.unlock();
                        resLock.unlock();

                        // This is were the resource is loaded.. the expensive part
                        if (workContent->m_resource->OnCreation(workContent->GetFileName())) {
                            // Creation successfull

                            { // Check if we really want to assign it.. or if join is requested
                                std::lock_guard<std::mutex> shutdownLock(m_shutdownMutex);
                                if (m_asyncShutdown) {
                                    break;
                                }
                            }

                            std::lock(resLock, asyncReqLock); // lock again, to check again, maybe it was created nonasync between loading and now
                            auto it = m_resources.find(workContent->GetFileName());
                            if (it != m_resources.end()) {
                                // So while we were loading the file, the file got loaded synchron by some allocator, we need to check again..
                                it->second->IncreaseUseCount(1);

                                if (workContent->GetAllocator()->IsFetchingAsync()) {
                                    workContent->GetAllocator()->PushToFunctionQueue(std::move(workContent->m_invokerFunction), workContent->m_resource.get());
                                }
                                else {
                                    workContent->m_invokerFunction(it->second.get());
                                }

                                workContent->GetAllocator()->OnAsyncResourceInsertion(workContent->GetFileName());
#ifdef HRESDEBUG
                                std::cout << "Increased use count async after file was created in no async way or duplicate async query" << std::endl;
#endif
                            }
                            else {
                                // The resource is still not existant, push it into resources
                                m_resources.insert(std::make_pair(workContent->GetFileName(), std::move(workContent->m_resource)));

                                auto itr = m_resources.find(workContent->GetFileName());
                                HResource* resource = itr->second.get();

                                if (!workContent->GetAllocator()->IsFetchingAsync()) {
                                    workContent->GetAllocator()->PushToFunctionQueue(std::move(workContent->m_invokerFunction), resource);
                                }
                                else {
                                    workContent->m_invokerFunction(resource);
                                }

                                workContent->GetAllocator()->OnAsyncResourceInsertion(workContent->GetFileName());
                            }
                        }
                        else {
                            // Resource Creation unsuccessful, someone deleted or accessed the file while we were trying to load it
                            // How should we handle this?
#ifdef HRESDEBUG
                            std::cerr << "Idle()->OnCreate() File for async load no longer existant?" << std::endl;
#endif
                            abort();
                        }
                    }
                }
            } // auto releases resLock and asyncReqLock

            lock.unlock();
        }
    }
}

bool HResourceManager::IsFileAlreadyAssigned(const std::string& fileName) {
    std::lock_guard<std::mutex> lock(m_resourcesMutex);
    auto it = m_resources.find(fileName);
    return it != m_resources.end();
}

} // namespace Res
} // namespace HE
